export interface TaskIdName {
    id: number;
    name: string;
}
