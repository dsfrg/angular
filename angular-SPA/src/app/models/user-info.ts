import { User } from './user';
import { Project } from './project';
import { Task } from './task';

export interface UserInfo {
    user: User;
    project: Project;
    countOfTasks: number;
    countOfUnfOrCnclTAsk: number;
    longestTask: Task;
}
