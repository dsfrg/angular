import { User } from './user';
import { Task } from './task';

export interface UserTask {
    user: User;
    tasks: Task[];
}
