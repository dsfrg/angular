import { TaskState } from './task-state.enum';
import { Project } from './project';
import { User } from './user';

export interface Task {
    id: number;
    name: string;
    description: string;
    createdAt: Date;
    finishedAt: Date;
    state: TaskState;
    project: Project;
    performer: User;
}
