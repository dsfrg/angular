import { Team } from './team';
import { UserRole } from './role';
import { Task } from './task';
import { Project } from './project';

export interface User {
    id: number;
    firstName: string;
    lastName: string;
    emailAddress: string;
    birthdayDate: Date;
    registeredAt: Date;
    team: Team;
    role: UserRole;

    tasks: Task[];
    projects: Project[];
}

