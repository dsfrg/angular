import { User } from './user';

export interface TeamUser {
    teamId: number;
    teamName: string;
    users: User[];
}
