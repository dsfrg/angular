import { User } from './user';

export interface UserRole {
    Id: number;
    role: string;
    users: User[];
}
