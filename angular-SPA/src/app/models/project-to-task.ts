import { Project } from './project';

export interface ProjectToTask {
    project: Project;
    countOfTasks: number;
}
