import { User } from './user';
import { Team } from './team';
import { Task } from './task';

export interface Project {
    id: number;
    name: string;
    description: string;
    createdAt: Date;
    deadline: Date;
    author: User;
    team: Team;
    tasks: Task[];
}
