import { Project } from './project';
import { Task } from './task';

export interface ProjectInfo {
    project: Project;
    longestTask: Task;
    shortestTask: Task;
    countOfUSers: number;
}
