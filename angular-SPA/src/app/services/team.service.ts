import { Injectable, EventEmitter } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Team } from '../models/team';

@Injectable({
  providedIn: 'root'
})
export class TeamService {
  public teamSelected: EventEmitter<number> = new EventEmitter<number>();
  public  appURL = environment.url;
  constructor(private httpService: HttpClient) { }

  public getTeams() {
    return this.httpService.get<Team[]>(this.appURL + 'team');
  }

  public getSingleTeam(id: number) {
    return this.httpService.get<Team>(this.appURL + `team/${id}`);
  }

  public addTeam(team: Team) {
    return this.httpService.post(this.appURL + 'team', team);
  }

  public deleteTeam(id: number) {
    return this.httpService.delete(this.appURL + `team/${id}`);
  }

  public updateTeam(team: Team) {
    return this.httpService.put(this.appURL + 'team', team);
  }
}
