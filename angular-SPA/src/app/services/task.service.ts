import { Injectable, EventEmitter } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Task } from '../models/task';

@Injectable({
  providedIn: 'root'
})
export class TaskService {
  public taskSelected: EventEmitter<number> = new EventEmitter<number>();
  public  appURL = environment.url;
  constructor(private httpService: HttpClient) { }

  public getTasks() {
    return this.httpService.get<Task[]>(this.appURL + 'task');
  }

  public getSingleTask(id: number) {
    return this.httpService.get<Task>(this.appURL + `task/${id}`);
  }

  public addtask(task: Task) {
    return this.httpService.post(this.appURL + 'task', task);
  }

  public deleteTask(id: number) {
    return this.httpService.delete(this.appURL + `task/${id}`);
  }

  public updateTask(task: Task) {
    return this.httpService.put(this.appURL + 'task', task);
  }
}
