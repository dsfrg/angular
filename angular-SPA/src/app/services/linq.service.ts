import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ProjectToTask } from '../models/project-to-task';
import { environment } from 'src/environments/environment';
import { Task } from '../models/task';
import { TaskIdName } from '../models/task-id-name';
import { TeamUser } from '../models/team-user';
import { UserTask } from '../models/user-task';
import { UserInfo } from '../models/user-info';
import { ProjectInfo } from '../models/project-info';

@Injectable({
  providedIn: 'root'
})
export class LinqService {

constructor(private httpService: HttpClient) { }

public task1(id: number) {
  return this.httpService.get<ProjectToTask[]>(environment.url + `query/projecttotask/${id}`);
}
public task2(id: number) {
  return this.httpService.get<Task[]>(environment.url + `query/usertaknameless45/${id}`);
}

public task3(id: number) {
  return this.httpService.get<TaskIdName[]>(environment.url + `query/finishedtasksidname/${id}`);
}

public task4() {
  return this.httpService.get<TeamUser[]>(environment.url + `query/teamswithusers`);
}
public task5() {
  return this.httpService.get<UserTask[]>(environment.url + `query/userswithtasks`);
}
public task6(id: number) {
  return this.httpService.get<UserInfo>(environment.url + `query/infoaboutuser/${id}`);
}
public task7() {
  return this.httpService.get<ProjectInfo[]>(environment.url + `query/GetProjectInfo}`);
}
}
