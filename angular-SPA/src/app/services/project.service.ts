import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Project } from '../models/project';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {
  public projectSelected: EventEmitter<Project> = new EventEmitter<Project>();
  public  appURL = environment.url;
  constructor(private httpService: HttpClient) { }

  public getProjects() {
    return this.httpService.get<Project[]>(this.appURL + 'project');
  }

  public getSingleProject(id: number) {
    return this.httpService.get<Project>(this.appURL + `project/${id}`);
  }

  public addProject(project: Project) {
    return this.httpService.post(this.appURL + 'project/add', project);
  }

  public deleteProject(id: number) {
    return this.httpService.delete(this.appURL + `project/${id}`);
  }

  public updateProject(project: Project) {
    return this.httpService.put(this.appURL + 'project', project);
  }

}
