import { Injectable, EventEmitter } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  public userSelected: EventEmitter<number> = new EventEmitter<number>();
  public  appURL = environment.url;
  constructor(private httpService: HttpClient) { }

  public getUsers() {
    return this.httpService.get<User[]>(this.appURL + 'user');
  }

  public getSingleUser(id: number) {
    return this.httpService.get<User>(this.appURL + `user/${id}`);
  }

  public addUser(user: User) {
    return this.httpService.post(this.appURL + 'user', user);
  }

  public deleteUser(id: number) {
    return this.httpService.delete(this.appURL + `user/${id}`);
  }

  public updateUser(user: User) {
    return this.httpService.put(this.appURL + 'user', user);
  }

}
