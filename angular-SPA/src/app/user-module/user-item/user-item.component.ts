import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user';
import { UserService } from 'src/app/services/user.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NotificationService } from 'src/app/services/notification.service';

@Component({
  selector: 'app-user-item',
  templateUrl: './user-item.component.html',
  styleUrls: ['./user-item.component.css']
})
export class UserItemComponent implements OnInit {
  public users: User[];
  constructor(
    private userService: UserService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private toastrService: NotificationService
    ) { }

  ngOnInit() {
    this.userService.getUsers()
    .subscribe((response) => this.users = response);
  }

  public itemClicked(id: number) {
    this.router.navigate(['detail', id], { relativeTo: this.activatedRoute});
  }

  public onAddClicked() {
    this.router.navigate(['new'], { relativeTo: this.activatedRoute});
  }

  public onEdit(id: number) {
    this.router.navigate(['edit', id], { relativeTo: this.activatedRoute});
  }

  public onDelete(id: number) {
    this.users = this.users.filter(x => x.id !== id);
    this.toastrService.showSuccess('Deleted soft', 'Soft');
  }
}
