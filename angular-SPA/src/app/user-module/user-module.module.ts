import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserModuleComponent } from './user-module.component';
import { BrowserModule } from '@angular/platform-browser';
import { UserRoutingModule } from './user-routing.module';
import { UserItemComponent } from './user-item/user-item.component';
import { UserDetailComponent } from './user-detail/user-detail.component';
import { ReactiveFormsModule } from '@angular/forms';
import { UserEditComponent } from './user-edit/user-edit.component';

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    UserRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [
    UserModuleComponent,
    UserItemComponent,
    UserDetailComponent,
    UserEditComponent
  ]
})
export class UserModuleModule { }
