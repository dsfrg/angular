import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { User } from 'src/app/models/user';
import { Router, ActivatedRoute } from '@angular/router';
import { NotificationService } from 'src/app/services/notification.service';
import { UserService } from 'src/app/services/user.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.css']
})
export class UserEditComponent implements OnInit {

  public formUser: FormGroup;
  public isEdit: boolean;
  public id: number;
  public user: User;
  public canLeave = false;
  constructor(
    private router: Router,
    private toastrService: NotificationService,
    private activatedRoute: ActivatedRoute,
    private userService: UserService) { }

  ngOnInit() {
    console.log('user edit');
    this.formUser = new FormGroup({
      firstName: new FormControl('', Validators.required),
      lastName: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.email])
    });

    const index = this.router.url.lastIndexOf('/');
    if (this.router.url.substring(index, this.router.url.length) === '/new') {
      this.isEdit = false;
    } else {
      this.isEdit = true;
      this.activatedRoute.params.subscribe(params => {
        this.id = params[`id`];
        this.userService.getSingleUser(this.id)
        .subscribe((user: User) => {
          this.user = user;
          console.log(this.user);
          this.formUser.setValue({
            firstName: this.user.firstName,
            lastName: this.user.lastName,
            email: this.user.emailAddress
          });
        });
      });
    }
  }

  canDeactivate(): boolean | Observable<boolean> | Promise<boolean>{
    if (this.canLeave){
      return true;
    }
    if (this.formUser.dirty)
    {
      return confirm('Do you want to leave unsaved changes?');
    }
    return true;
  }


  onSubmit() {
    if (this.isEdit) {
      this.user.firstName = this.formUser.value.firstName;
      this.user.lastName = this.formUser.value.lastName;
      this.user.emailAddress = this.formUser.value.email;
      this.userService.updateUser(this.user).subscribe(x => {
         this.toastrService.showSuccess('User updated', 'Info');
         this.canLeave = true;
         this.router.navigate(['/user']);
      });
    } else {
      const userToAdd: User = {
        id: -1,
        firstName: this.formUser.value.firstName,
        lastName: this.formUser.value.lastName,
        emailAddress: this.formUser.value.email,
        birthdayDate: new Date(),
        registeredAt: new Date(),
        team: null,
        projects: [],
        tasks: [],
        role: null
      };
      this.userService.addUser(userToAdd).subscribe(response => {
        this.toastrService.showSuccess('User added', 'Success');
        this.canLeave = true;
        this.router.navigate(['/user']);
      }, error => {
        this.toastrService.showError('User is not added', 'Failed');
        this.canLeave = true;
        this.router.navigate(['/user']);
      }
      );
    }
  }


}
