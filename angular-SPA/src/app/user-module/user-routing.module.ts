import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserModuleComponent } from './user-module.component';
import { UserItemComponent } from './user-item/user-item.component';
import { UserDetailComponent } from './user-detail/user-detail.component';
import { UserEditComponent } from './user-edit/user-edit.component';


const routes: Routes = [
    {path: 'user', component: UserModuleComponent, children: [
        {path: '', component: UserItemComponent},
        {path: 'list', component: UserItemComponent},
        {path: 'detail/:id', component: UserDetailComponent},
        {path: 'new', component: UserEditComponent},
        {path: 'edit/:id', component: UserEditComponent}
      ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
