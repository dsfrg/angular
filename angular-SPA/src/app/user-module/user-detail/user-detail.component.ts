import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user';
import { Subject } from 'rxjs';
import { UserService } from 'src/app/services/user.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css']
})
export class UserDetailComponent implements OnInit {

  public user: User = {} as User;
  public id: number;
  public unsubscribe$ = new Subject<void>();
  constructor(private userService: UserService, private router: ActivatedRoute) { }

  ngOnInit() {
   this.router.params.subscribe(params => {
      this.id = params[`id`];
      this.userService.getSingleUser(this.id)
    .subscribe((user: User) => {
      this.user = user;
    });
    });
  }
}
