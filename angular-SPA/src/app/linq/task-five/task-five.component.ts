import { Component, OnInit } from '@angular/core';
import { LinqService } from 'src/app/services/linq.service';
import { UserTask } from 'src/app/models/user-task';
import { NotificationService } from 'src/app/services/notification.service';

@Component({
  selector: 'app-task-five',
  templateUrl: './task-five.component.html',
  styleUrls: ['./task-five.component.css']
})
export class TaskFiveComponent implements OnInit {
  data: UserTask[];
  canShow = false;
  constructor(private linqService: LinqService, private toastrService: NotificationService) { }

  ngOnInit() {
  }

  public show() {
    this.linqService.task5().subscribe(x => { this.data = x; console.log(x); this.canShow = true; },
    error => this.toastrService.showError('Internal server error', 'Failed'));
  }
}
