import { Component, OnInit } from '@angular/core';
import { LinqService } from 'src/app/services/linq.service';
import { NotificationService } from 'src/app/services/notification.service';
import { ProjectInfo } from 'src/app/models/project-info';

@Component({
  selector: 'app-task-seven',
  templateUrl: './task-seven.component.html',
  styleUrls: ['./task-seven.component.css']
})
export class TaskSevenComponent implements OnInit {

  data: ProjectInfo[];
  canShow = false;
  constructor(private linqService: LinqService, private tService: NotificationService) { }

  ngOnInit() {
  }

  public show() {
    this.linqService.task7().subscribe(x => { this.data = x; console.log(x); this.canShow = true;},
    error => {
      this.tService.showError('Failed to load data', 'Failed');
    });
  }
}
