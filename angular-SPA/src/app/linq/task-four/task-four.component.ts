import { Component, OnInit } from '@angular/core';
import { LinqService } from 'src/app/services/linq.service';
import { TeamUser } from 'src/app/models/team-user';

@Component({
  selector: 'app-task-four',
  templateUrl: './task-four.component.html',
  styleUrls: ['./task-four.component.css']
})
export class TaskFourComponent implements OnInit {



  data: TeamUser[];
  canShow = false;
  constructor(private linqService: LinqService) { }

  ngOnInit() {
  }

  public show() {
    this.linqService.task4().subscribe(x => { this.data = x; console.log(x); this.canShow = true; });
  }

}
