import { Component, OnInit } from '@angular/core';
import { Task } from 'src/app/models/task';
import { LinqService } from 'src/app/services/linq.service';

@Component({
  selector: 'app-task-two',
  templateUrl: './task-two.component.html',
  styleUrls: ['./task-two.component.css']
})
export class TaskTwoComponent implements OnInit {

  data: Task[];
  canShow = false;
  constructor(private linqService: LinqService) { }

  ngOnInit() {
  }

  public show(value: number) {
    this.linqService.task2(value).subscribe(x => { this.data = x; console.log(x); this.canShow = true; });
  }
}
