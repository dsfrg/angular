import { Component, OnInit } from '@angular/core';
import { LinqService } from 'src/app/services/linq.service';
import { UserInfo } from 'src/app/models/user-info';
import { NotificationService } from 'src/app/services/notification.service';


@Component({
  selector: 'app-task-six',
  templateUrl: './task-six.component.html',
  styleUrls: ['./task-six.component.css']
})
export class TaskSixComponent implements OnInit {

  data: UserInfo;
  canShow = false;
  constructor(private linqService: LinqService, private tService: NotificationService) { }

  ngOnInit() {
  }

  public show(value: number) {
    this.linqService.task6(value).subscribe(x => { this.data = x; console.log(x); this.canShow = true;},
    error => {
      this.tService.showError('Failed to load data', 'Failed');
    });
  }
}
