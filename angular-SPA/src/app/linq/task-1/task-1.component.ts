import { Component, OnInit, ElementRef } from '@angular/core';
import { ProjectToTask } from 'src/app/models/project-to-task';
import { LinqService } from 'src/app/services/linq.service';

@Component({
  selector: 'app-task-1',
  templateUrl: './task-1.component.html',
  styleUrls: ['./task-1.component.css']
})
export class TaskOneComponent implements OnInit {
  data: ProjectToTask[];
  canShow = false;
  constructor(private linqService: LinqService) { }

  ngOnInit() {
  }

  public show(value: number) {
    console.log('i am');
    this.linqService.task1(value).subscribe(x => { this.data = x; console.log(x); this.canShow = true;});
  }

}
