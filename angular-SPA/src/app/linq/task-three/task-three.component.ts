import { Component, OnInit } from '@angular/core';
import { LinqService } from 'src/app/services/linq.service';
import { TaskIdName } from 'src/app/models/task-id-name';

@Component({
  selector: 'app-task-three',
  templateUrl: './task-three.component.html',
  styleUrls: ['./task-three.component.css']
})
export class TaskThreeComponent implements OnInit {


  data: TaskIdName[];
  canShow = false;
  constructor(private linqService: LinqService) { }

  ngOnInit() {
  }

  public show(value: number) {
    this.linqService.task3(value).subscribe(x => { this.data = x; console.log(x); this.canShow = true; });
  }
}
