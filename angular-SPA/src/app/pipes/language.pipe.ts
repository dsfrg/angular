import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'language'
})
export class LanguagePipe implements PipeTransform {

  transform(value: string, args?: any): string {
    const months =
  [
    'січeнь', 'лютий', 'березень',
    'квітень', 'травень', 'червень',
    'липень', 'серпень', 'вересень',
    'жовтень', 'листопад', 'грудень'
  ];
    const month = +value.split('-')[1];
    const day  = value.split('-')[2].substring(0, 2);
    const year = value.split('-')[0];
    return `${day}  ${months[month - 1]} ${year}`;
  }

}
