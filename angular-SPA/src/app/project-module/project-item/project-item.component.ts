import { Component, OnInit } from '@angular/core';
import { Project } from 'src/app/models/project';
import { ProjectService } from 'src/app/services/project.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NotificationService } from 'src/app/services/notification.service';
import { LanguagePipe } from 'src/app/pipes/language.pipe';

@Component({
  selector: 'app-project-item',
  templateUrl: './project-item.component.html',
  styleUrls: ['./project-item.component.css']
})
export class ProjectItemComponent implements OnInit {
  public projects: Project[];
  constructor(
    private projectService: ProjectService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private toastrService: NotificationService,
    pipe: LanguagePipe
    ) { }

  ngOnInit() {
    this.projectService.getProjects()
    .subscribe((response) => this.projects = response);
  }

  public itemClicked(id: number) {
    this.router.navigate(['detail', id], { relativeTo: this.activatedRoute});
  }

  public onAddClicked() {
    this.router.navigate(['new'], { relativeTo: this.activatedRoute});
  }

  public onEdit(id: number) {
    this.router.navigate(['edit', id], { relativeTo: this.activatedRoute});
  }

  public onDelete(id: number) {
    this.projects = this.projects.filter(x => x.id !== id);
    this.toastrService.showSuccess('Deleted soft', 'Soft');
  }
}
