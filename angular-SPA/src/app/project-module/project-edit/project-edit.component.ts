import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { NotificationService } from 'src/app/services/notification.service';
import { ProjectService } from 'src/app/services/project.service';
import { Project } from 'src/app/models/project';
import { CanComponentDeactivate } from 'src/app/guards/canleave.guard';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-project-edit',
  templateUrl: './project-edit.component.html',
  styleUrls: ['./project-edit.component.css']
})
export class ProjectEditComponent implements OnInit, CanComponentDeactivate {
  public formProject: FormGroup;
  public isEdit: boolean;
  public id: number;
  public project: Project;
  public canLeave = false;
  constructor(
    private router: Router,
    private toastrService: NotificationService,
    private activatedRoute: ActivatedRoute,
    private projectService: ProjectService) { }

  ngOnInit() {
    this.formProject = new FormGroup({
      name: new FormControl('', Validators.required),
      description: new FormControl('', [Validators.required, Validators.maxLength(100)]),
      deadline: new FormControl()
    });

    const index = this.router.url.lastIndexOf('/');
    if (this.router.url.substring(index, this.router.url.length) === '/new') {
      this.isEdit = false;
    } else {
      this.isEdit = true;
      this.activatedRoute.params.subscribe(params => {
        this.id = params[`id`];
        this.projectService.getSingleProject(this.id)
        .subscribe((project: Project) => {
          this.project = project;
          this.formProject.setValue({
            name: this.project.name,
            description: this.project.description,
            deadline: this.project.deadline
          });
        });
      });
    }
  }

  canDeactivate(): boolean | Observable<boolean> | Promise<boolean>{
    if (this.canLeave){
      return true;
    }
    if (this.formProject.dirty)
    {
      return confirm('Do you want to leave unsaved changes?');
    }
    return true;
  }


  onSubmit() {
    if (this.isEdit) {
      this.project.name = this.formProject.value.name;
      this.project.description = this.formProject.value.description;
      this.project.deadline = this.formProject.value.deadline;
      this.projectService.updateProject(this.project).subscribe(x => {
         this.toastrService.showSuccess('Project updated', 'Info');
         this.canLeave = true;
         this.router.navigate(['/project']);
      });
    } else {
      const projectToAdd: Project = {
        id: -1,
        name: this.formProject.value.name,
        description: this.formProject.value.description,
        createdAt: new Date(),
        deadline: this.formProject.value.deadline,
        author: null,
        team: null,
        tasks: []
      };
      this.projectService.addProject(projectToAdd).subscribe(response => {
        this.toastrService.showSuccess('Project added', 'Success');
        this.canLeave = true;
        this.router.navigate(['/project']);
      }, error => {
        this.toastrService.showError('Project is not added', 'Failed');
        this.canLeave = true;
        this.router.navigate(['/project']);
      }
      );
    }
  }

}
