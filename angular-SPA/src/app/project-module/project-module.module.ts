import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProjectModuleComponent } from './project-module.component';
import { BrowserModule } from '@angular/platform-browser';
import { ProjectRoutingModule } from './project-routing.module';
import { ProjectDetailComponent } from './project-detail/project-detail.component';
import { ProjectItemComponent } from './project-item/project-item.component';
import { ProjectEditComponent } from './project-edit/project-edit.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LanguagePipe } from '../pipes/language.pipe';


@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    ProjectRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
    ProjectModuleComponent,
    ProjectDetailComponent,
    ProjectItemComponent,
    ProjectEditComponent,
    LanguagePipe
  ]
})
export class ProjectModuleModule { }
