import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProjectItemComponent } from './project-item/project-item.component';
import { ProjectDetailComponent } from './project-detail/project-detail.component';
import { ProjectEditComponent } from './project-edit/project-edit.component';
import { ProjectModuleComponent } from './project-module.component';
import { DeactivateGuardService } from '../guards/canleave.guard';


const routes: Routes = [
    {path: 'project', component: ProjectModuleComponent, children: [
        {path: '', component: ProjectItemComponent},
        {path: 'list', component: ProjectItemComponent},
        {path: 'detail/:id', component: ProjectDetailComponent},
        {path: 'new', component: ProjectEditComponent, canDeactivate: [DeactivateGuardService]},
        {path: 'edit/:id', component: ProjectEditComponent, canDeactivate: [DeactivateGuardService]}
      ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProjectRoutingModule { }
