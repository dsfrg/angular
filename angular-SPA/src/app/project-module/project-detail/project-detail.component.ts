import { Component, OnInit } from '@angular/core';
import { ProjectService } from 'src/app/services/project.service';
import { Project } from 'src/app/models/project';
import { Router, ActivatedRoute } from '@angular/router';
import { User } from 'src/app/models/user';
import { Team } from 'src/app/models/team';
import { Subject } from 'rxjs';
import { switchMap, takeUntil, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-project-detail',
  templateUrl: './project-detail.component.html',
  styleUrls: ['./project-detail.component.css']
})
export class ProjectDetailComponent implements OnInit {
  public proj: Project = {} as Project;
  public id: number;
  public unsubscribe$ = new Subject<void>();
  constructor(private projectService: ProjectService, private router: ActivatedRoute, private userS: UserService) { }

  ngOnInit() {
   const one  = this.router.params.subscribe(params => {
      this.id = params[`id`];
      this.projectService.getSingleProject(this.id)
    .subscribe((project: Project) => {
      this.proj = project;
    });
    });
  }
  change() {
    console.log(this.proj.author.birthdayDate);
  }
}
