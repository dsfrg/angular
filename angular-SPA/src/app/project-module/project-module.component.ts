import { Component, OnInit } from '@angular/core';
import { NotificationService } from '../services/notification.service';

@Component({
  selector: 'app-project-module',
  templateUrl: './project-module.component.html',
  styleUrls: ['./project-module.component.css']
})
export class ProjectModuleComponent implements OnInit {

  constructor(private toastrService: NotificationService) { }

  ngOnInit() {
    this.toastrService.showInfo('Click to item to see detailed info', 'Info');
  }

}
