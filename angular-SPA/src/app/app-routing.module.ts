import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StarterComponent } from './starter/starter.component';
import { LinqComponent } from './linq/linq.component';
import { TaskOneComponent } from './linq/task-1/task-1.component';
import { TaskTwoComponent } from './linq/task-two/task-two.component';
import { TaskThreeComponent } from './linq/task-three/task-three.component';
import { TaskFourComponent } from './linq/task-four/task-four.component';
import { TaskFiveComponent } from './linq/task-five/task-five.component';
import { TaskSixComponent } from './linq/task-six/task-six.component';
import { TaskSevenComponent } from './linq/task-seven/task-seven.component';


const routes: Routes = [
  {path: '', component: StarterComponent},
  {path: 'project', redirectTo: '/project', pathMatch: 'full'},
  {path: 'user', redirectTo: '/user', pathMatch: 'full'},
  {path: 'task', redirectTo: '/task', pathMatch: 'full'},
  {path: 'team', redirectTo: '/team', pathMatch: 'full'},
  {path: 'linq', component: LinqComponent, children: [
    {path: 'task1', component: TaskOneComponent},
    {path: 'task2', component: TaskTwoComponent},
    {path: 'task3', component: TaskThreeComponent},
    {path: 'task4', component: TaskFourComponent},
    {path: 'task5', component: TaskFiveComponent},
    {path: 'task6', component: TaskSixComponent},
    {path: 'task7', component: TaskSevenComponent},
  ]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
