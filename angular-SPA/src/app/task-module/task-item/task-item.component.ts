import { Component, OnInit } from '@angular/core';
import { Task } from 'src/app/models/task';
import { TaskService } from 'src/app/services/task.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NotificationService } from 'src/app/services/notification.service';

@Component({
  selector: 'app-task-item',
  templateUrl: './task-item.component.html',
  styleUrls: ['./task-item.component.css']
})
export class TaskItemComponent implements OnInit {

  public tasks: Task[];
  constructor(
    private taskService: TaskService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private toastrService: NotificationService
    ) { }

  ngOnInit() {
    this.taskService.getTasks()
    .subscribe((response) => this.tasks = response);
  }

  public itemClicked(id: number) {
    this.router.navigate(['detail', id], { relativeTo: this.activatedRoute});
  }

  public onAddClicked() {
    this.router.navigate(['new'], { relativeTo: this.activatedRoute});
  }

  public onEdit(id: number) {
    this.router.navigate(['edit', id], { relativeTo: this.activatedRoute});
  }

  public onDelete(id: number) {
    this.tasks = this.tasks.filter(x => x.id !== id);
    this.toastrService.showSuccess('Deleted soft', 'Soft');
  }
}
