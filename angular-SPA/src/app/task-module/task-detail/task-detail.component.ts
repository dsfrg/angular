import { Component, OnInit } from '@angular/core';
import { Task } from 'src/app/models/task';
import { Subject } from 'rxjs';
import { TaskService } from 'src/app/services/task.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-task-detail',
  templateUrl: './task-detail.component.html',
  styleUrls: ['./task-detail.component.css']
})
export class TaskDetailComponent implements OnInit {

  public task: Task = {} as Task;
  public id: number;
  public unsubscribe$ = new Subject<void>();
  constructor(private taskService: TaskService, private router: ActivatedRoute) { }

  ngOnInit() {
   const one  = this.router.params.subscribe(params => {
      this.id = params[`id`];
      this.taskService.getSingleTask(this.id)
    .subscribe((task: Task) => {
      this.task = task;
    });
    });
  }
}
