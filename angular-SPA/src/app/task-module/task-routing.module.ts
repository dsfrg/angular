import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TaskModuleComponent } from './task-module.component';
import { TaskItemComponent } from './task-item/task-item.component';
import { TaskDetailComponent } from './task-detail/task-detail.component';
import { TaskEditComponent } from './task-edit/task-edit.component';


const routes: Routes = [
    {path: 'task', component: TaskModuleComponent, children: [
        {path: '', component: TaskItemComponent},
        {path: 'list', component: TaskItemComponent},
        {path: 'detail/:id', component: TaskDetailComponent},
        {path: 'new', component: TaskEditComponent},
        {path: 'edit/:id', component: TaskEditComponent}
      ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TaskRoutingModule { }
