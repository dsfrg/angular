import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TaskModuleComponent } from './task-module.component';
import { TaskRoutingModule } from './task-routing.module';
import { TaskItemComponent } from './task-item/task-item.component';
import { TaskEditComponent } from './task-edit/task-edit.component';
import { TaskDetailComponent } from './task-detail/task-detail.component';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { TaskDirectiveDirective } from '../directives/task-directive.directive';

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    TaskRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [
    TaskModuleComponent,
    TaskItemComponent,
    TaskEditComponent,
    TaskDetailComponent,
    TaskDirectiveDirective
  ]
})
export class TaskModuleModule { }
