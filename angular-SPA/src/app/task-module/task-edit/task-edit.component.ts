import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Task } from 'src/app/models/task';
import { Router, ActivatedRoute } from '@angular/router';
import { NotificationService } from 'src/app/services/notification.service';
import { TaskService } from 'src/app/services/task.service';
import { Observable } from 'rxjs';
import { TaskState } from 'src/app/models/task-state.enum';

@Component({
  selector: 'app-task-edit',
  templateUrl: './task-edit.component.html',
  styleUrls: ['./task-edit.component.css']
})
export class TaskEditComponent implements OnInit {
  public states = [TaskState.Created, TaskState.Started, TaskState.Canceled, TaskState.Finished];
  public statesName = ['created', 'started', 'canceled', 'finished'];
  public taskForm: FormGroup;
  public isEdit: boolean;
  public id: number;
  public task: Task;
  public canLeave = false;
  constructor(
    private router: Router,
    private toastrService: NotificationService,
    private activatedRoute: ActivatedRoute,
    private taskService: TaskService) { }

  ngOnInit() {
    this.taskForm = new FormGroup({
      name: new FormControl('', Validators.required),
      description: new FormControl('', [Validators.required, Validators.maxLength(100)]),
      state: new FormControl()
    });

    const index = this.router.url.lastIndexOf('/');
    if (this.router.url.substring(index, this.router.url.length) === '/new') {
      this.isEdit = false;
    } else {
      this.isEdit = true;
      this.activatedRoute.params.subscribe(params => {
        this.id = params[`id`];
        this.taskService.getSingleTask(this.id)
        .subscribe((task: Task) => {
          this.task = task;
          this.taskForm.setValue({
            name: this.task.name,
            description: this.task.description,
            state: this.task.state
          });
        });
      });
    }
  }

  canDeactivate(): boolean | Observable<boolean> | Promise<boolean>{
    if (this.canLeave){
      return true;
    }
    if (this.taskForm.dirty)
    {
      return confirm('Do you want to leave unsaved changes?');
    }
    return true;
  }


  onSubmit() {
    if (this.isEdit) {
      this.task.name = this.taskForm.value.name;
      this.task.description = this.taskForm.value.description;
      this.task.state = this.taskForm.value.state;
      this.taskService.updateTask(this.task).subscribe(x => {
         this.toastrService.showSuccess('Task updated', 'Info');
         this.canLeave = true;
         this.router.navigate(['/task']);
      });
    } else {
      const taskToAdd: Task = {
        id: -1,
        name: this.taskForm.value.name,
        description: this.taskForm.value.description,
        createdAt: new Date(),
        finishedAt: new Date(),
        performer: null,
        project: null,
        state: this.taskForm.value.state
      };
      this.taskService.addtask(taskToAdd).subscribe(response => {
        this.toastrService.showSuccess('Task added', 'Success');
        this.canLeave = true;
        this.router.navigate(['/task']);
      }, error => {
        this.toastrService.showError('Task is not added', 'Failed');
        this.canLeave = true;
        this.router.navigate(['/task']);
      }
      );
    }
  }

}
