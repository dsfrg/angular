import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProjectModuleModule } from './project-module/project-module.module';
import { TaskModuleModule } from './task-module/task-module.module';
import { TeamModuleModule } from './team-module/team-module.module';
import { UserModuleModule } from './user-module/user-module.module';
import { StarterComponent } from './starter/starter.component';
import { HttpClientModule } from '@angular/common/http';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DeactivateGuardService } from './guards/canleave.guard';
import { LanguagePipe } from './pipes/language.pipe';
import { LinqComponent } from './linq/linq.component';
import { TaskOneComponent } from './linq/task-1/task-1.component';
import { TaskTwoComponent } from './linq/task-two/task-two.component';
import { TaskThreeComponent } from './linq/task-three/task-three.component';
import { TaskFourComponent } from './linq/task-four/task-four.component';
import { TaskFiveComponent } from './linq/task-five/task-five.component';
import { TaskSixComponent } from './linq/task-six/task-six.component';
import { TaskSevenComponent } from './linq/task-seven/task-seven.component';
import { TaskDirectiveDirective } from './directives/task-directive.directive';
@NgModule({
   declarations: [
      AppComponent,
      StarterComponent,
      LinqComponent,
      TaskOneComponent,
      TaskTwoComponent,
      TaskThreeComponent,
      TaskFourComponent,
      TaskFiveComponent,
      TaskSixComponent,
      TaskSevenComponent
   ],
   imports: [
      BrowserModule,
      BrowserAnimationsModule,
      AppRoutingModule,
      ProjectModuleModule,
      TaskModuleModule,
      TeamModuleModule,
      UserModuleModule,
      HttpClientModule,
      ToastrModule.forRoot(),
      FormsModule,
      ReactiveFormsModule
   ],
   providers: [DeactivateGuardService, LanguagePipe],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule { }
