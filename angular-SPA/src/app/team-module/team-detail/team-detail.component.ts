import { Component, OnInit } from '@angular/core';
import { Team } from 'src/app/models/team';
import { Subject } from 'rxjs';
import { TeamService } from 'src/app/services/team.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-team-detail',
  templateUrl: './team-detail.component.html',
  styleUrls: ['./team-detail.component.css']
})
export class TeamDetailComponent implements OnInit {
  public team: Team = {} as Team;
  public id: number;
  public unsubscribe$ = new Subject<void>();
  constructor(private teamService: TeamService, private router: ActivatedRoute) { }

  ngOnInit() {
    this.router.params.subscribe(params => {
      this.id = params[`id`];
      this.teamService.getSingleTeam(this.id)
    .subscribe((team: Team) => {
      this.team = team;
    });
    });
  }
}
