import { Component, OnInit } from '@angular/core';
import { CanComponentDeactivate } from 'src/app/guards/canleave.guard';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Team } from 'src/app/models/team';
import { Router, ActivatedRoute } from '@angular/router';
import { NotificationService } from 'src/app/services/notification.service';
import { TeamService } from 'src/app/services/team.service';
import { Observable } from 'rxjs/internal/Observable';

@Component({
  selector: 'app-team-edit',
  templateUrl: './team-edit.component.html',
  styleUrls: ['./team-edit.component.css']
})
export class TeamEditComponent implements OnInit, CanComponentDeactivate {
  public formTeam: FormGroup;
  public isEdit: boolean;
  public id: number;
  public team: Team;

  public canLeaveWithoutAsk = false;
  constructor(
    private router: Router,
    private toastrService: NotificationService,
    private activatedRoute: ActivatedRoute,
    private teamService: TeamService) { }

  ngOnInit() {
    this.formTeam = new FormGroup({
      name: new FormControl('', Validators.required),
    });

    const index = this.router.url.lastIndexOf('/');
    if (this.router.url.substring(index, this.router.url.length) === '/new') {
      this.isEdit = false;
    } else {
      this.isEdit = true;
      this.activatedRoute.params.subscribe(params => {
        this.id = params[`id`];
        this.teamService.getSingleTeam(this.id)
        .subscribe((team: Team) => {
          this.team = team;
          console.log(this.team);
          this.formTeam.setValue({
            name: this.team.name,
          });
        });
      });
    }
  }

  canDeactivate() : boolean | Observable<boolean> | Promise<boolean>{
    if(this.canLeaveWithoutAsk){
      return true;
    }
    if (this.formTeam.dirty)
    {
      return confirm('Do you want to leave unsaved changes?');
    }
    return true;
  }


  onSubmit() {
    if (this.isEdit) {
      this.team.name = this.formTeam.value.name;

      this.teamService.updateTeam(this.team).subscribe(x => { 
        this.toastrService.showSuccess('Team updated', 'Info');
        this.canLeaveWithoutAsk = true;
        this.router.navigate(['/team']);
      });
    } else {
      const teamToAdd: Team = {
        id: -1,
        name: this.formTeam.value.name,
        createdAt: new Date(),
        users: [],
        projects: []
      };
      this.teamService.addTeam(teamToAdd).subscribe(response => {
        this.toastrService.showSuccess('Team added', 'Success');
        this.canLeaveWithoutAsk = true;
        this.router.navigate(['/team']);
      }, error => {
        this.toastrService.showError('Team is not added', 'Failed');
        this.canLeaveWithoutAsk = true;
        this.router.navigate(['/team']);
      }
      );
    }
  }


}
