import { Component, OnInit } from '@angular/core';
import { Team } from 'src/app/models/team';
import { TeamService } from 'src/app/services/team.service';
import { Router, ActivatedRoute } from '@angular/router';
import { NotificationService } from 'src/app/services/notification.service';
import { LanguagePipe } from 'src/app/pipes/language.pipe';

@Component({
  selector: 'app-team-item',
  templateUrl: './team-item.component.html',
  styleUrls: ['./team-item.component.css']
})
export class TeamItemComponent implements OnInit {
  public teams: Team[];
  constructor(
    private teamService: TeamService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private toastrService: NotificationService
    ) { }

  ngOnInit() {
    this.teamService.getTeams()
    .subscribe((response) => this.teams = response);
  }

  public itemClicked(id: number) {
    this.router.navigate(['detail', id], { relativeTo: this.activatedRoute});
  }

  public onAddClicked() {
    this.router.navigate(['new'], { relativeTo: this.activatedRoute});
  }

  public onEdit(id: number) {
    this.router.navigate(['edit', id], { relativeTo: this.activatedRoute});
  }

  public onDelete(id: number) {
    this.teams = this.teams.filter(x => x.id !== id);
    this.toastrService.showSuccess('Deleted soft', 'Soft');
  }
}
