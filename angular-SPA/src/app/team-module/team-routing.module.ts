import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TeamModuleComponent } from './team-module.component';
import { TeamItemComponent } from './team-item/team-item.component';
import { TeamDetailComponent } from './team-detail/team-detail.component';
import { TeamEditComponent } from './team-edit/team-edit.component';
import { DeactivateGuardService } from '../guards/canleave.guard';


const routes: Routes = [
    {path: 'team', component: TeamModuleComponent, children: [
        {path: '', component: TeamItemComponent},
        {path: 'list', component: TeamItemComponent},
        {path: 'detail/:id', component: TeamDetailComponent},
        {path: 'new', component: TeamEditComponent, canDeactivate: [DeactivateGuardService]},
        {path: 'edit/:id', component: TeamEditComponent, canDeactivate: [DeactivateGuardService]}
      ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TeamRoutingModule { }
