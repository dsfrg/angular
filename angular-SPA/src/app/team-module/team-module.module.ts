import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TeamModuleComponent } from './team-module.component';
import { BrowserModule } from '@angular/platform-browser';
import { TeamRoutingModule } from './team-routing.module';
import { TeamEditComponent } from './team-edit/team-edit.component';
import { TeamDetailComponent } from './team-detail/team-detail.component';
import { TeamItemComponent } from './team-item/team-item.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    TeamRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [
    TeamModuleComponent,
    TeamEditComponent,
    TeamDetailComponent,
    TeamItemComponent
  ]
})
export class TeamModuleModule { }
