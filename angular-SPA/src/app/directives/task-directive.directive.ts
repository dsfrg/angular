import { Directive, ElementRef, Input, HostListener } from '@angular/core';
import { TaskState } from '../models/task-state.enum';

@Directive({
  selector: '[appTaskDirective]'
})
export class TaskDirectiveDirective {
  @Input() taskStatus: TaskState;
  elRef: ElementRef;
  constructor(el: ElementRef) {
    this.elRef = el;
 }
  @HostListener('mouseover') onMouse() {
    this.setColor('green');
  }

  @HostListener('mouseleave') onMouseLeave() {
    this.setColor(null);
  }

  private setColor(color: string) {
    if (this.taskStatus === TaskState.Finished) {
      this.elRef.nativeElement.style.backgroundColor = color;
  }
}

}
