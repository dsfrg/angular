﻿using Common.DTOs;
using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;

namespace BLL.Profiles
{
    public class UserRoleProfile: Profile
    {
        public UserRoleProfile()
        {
            CreateMap<Task, TaskDto>().ReverseMap();
        }
    }
}
