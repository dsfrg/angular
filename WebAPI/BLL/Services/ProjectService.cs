﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using Common.DTOs;
using DAL.Entities;
using DAL.Repository;
using TaskThreading = System.Threading.Tasks;

namespace BLL.Services
{
    public class ProjectService
    {
        private readonly IRepository<Project> _projectRepo;
        private readonly IMapper _mapper;

        public ProjectService(IRepository<Project> repo, IMapper mapper)
        {
            _projectRepo = repo;
            _mapper = mapper;
        }

        public async TaskThreading.Task AddProject(ProjectDto item)
        {
            await _projectRepo.Add(_mapper.Map<Project>(item));
        }

        public async TaskThreading.Task AddRangeOfProjects(IEnumerable<ProjectDto> range)
        {
            await _projectRepo.AddRange(_mapper.Map<IEnumerable<Project>>(range));
        }

        public async TaskThreading.Task Delete(ProjectDto item)
        {
            await _projectRepo.Delete(_mapper.Map<Project>(item));
        }

        public async TaskThreading.Task DeleteById(int id)
        {
            await _projectRepo.DeleteById(id);
        }

        public async TaskThreading.Task<ProjectDto> GetProject(int id)
        {
            return  _mapper.Map<ProjectDto>(await _projectRepo.GetItem(id));
        }

        public async TaskThreading.Task<IEnumerable<ProjectDto>> GetProjects()
        {
            return _mapper.Map<IEnumerable<ProjectDto>>(await _projectRepo.GetItems());
        }

        public async TaskThreading.Task Update(ProjectDto item)
        {
            await _projectRepo.Update(_mapper.Map<Project>(item));
        }
    }
}
