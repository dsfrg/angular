﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Entities
{
    public class User
    {
        public int Id { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? EmailAddress { get; set; }

        public DateTime BirthdayDate { get; set; }
        public DateTime RegisteredAt { get; set; }

        public int? TeamId { get; set; }
        public Team Team { get; set; }

        public int RoleId { get; set; }
        public UserRole Role { get; set; }


        public virtual ICollection<Task> Tasks { get; set; }
        public virtual ICollection<Project> Projects { get; set; }

    }
}
