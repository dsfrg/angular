﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Entities
{
    public class HierarchyData
    {
        public int Id { get; set; }
        public string? Name { get; set; }

        public string? Description { get; set; }

        public DateTime CreatedAt { get; set; }
        public DateTime Deadline { get; set; }

        public int AuthorId { get; set; }
        public User Author { get; set; }
        public int TeamId { get; set; }
        public Team Team { get; set; }
        public List<Task> Tasks { get; set; }
    }
}
