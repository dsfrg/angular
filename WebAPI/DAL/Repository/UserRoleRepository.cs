﻿using DAL.Context;
using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TaskThreading = System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace DAL.Repository
{
    public class UserRoleRepository: IRepository<UserRole>
    {
        private readonly InitialDataContext _context;

        public UserRoleRepository(InitialDataContext context)
        {
            _context = context;
        }
        public async TaskThreading.Task<UserRole> GetItem(int id)
        {
            var role = await _context.UserRoles.FirstOrDefaultAsync(x => x.Id == id);
            if (role == null)
                throw new ArgumentException("user");

            return role;
        }

        public async TaskThreading.Task Add(UserRole item)
        {
            if (item is null)
                throw new ArgumentNullException("item");


            var role = await _context.UserRoles.FirstOrDefaultAsync(x => x.Id == item.Id);
            if (role != null)
            {
                var nextIndex = _context.UserRoles.OrderBy(x => x.Id).Last().Id;
                item.Id = ++nextIndex;
            }

            await _context.UserRoles.AddAsync(item);
            await _context.SaveChangesAsync();
        }

        public async TaskThreading.Task AddRange(IEnumerable<UserRole> range)
        {
            if (range is null)
                throw new ArgumentNullException("range");

            foreach (var item in range)
            {
                if (await _context.UserRoles.AnyAsync(x => x.Id == item.Id))
                    throw new ArgumentException("range");
            }

            await _context.UserRoles.AddRangeAsync(range);
            await _context.SaveChangesAsync();
        }

        public async TaskThreading.Task Update(UserRole item)
        {
            var role = await _context.UserRoles.FirstOrDefaultAsync(x => x.Id == item.Id);
            if (role == null)
                throw new ArgumentException("id");

            // it is must have, cause ef throws exeption that instance of entity type cannot be tracked 
            // because another instance with the same key value for {'Id'} is already being tracked
            _context.Entry<UserRole>(role).State = EntityState.Detached;
            _context.Entry<UserRole>(item).State = EntityState.Detached;

            _context.UserRoles.Update(item);
            await _context.SaveChangesAsync();
        }

        public async TaskThreading.Task Delete(UserRole item)
        {
            if (item is null)
                throw new ArgumentNullException("item");

            _context.UserRoles.Remove(item);
            await _context.SaveChangesAsync();
        }

        public async TaskThreading.Task DeleteById(int id)
        {
            var role = await _context.UserRoles.FirstOrDefaultAsync(x => x.Id == id);
            if (role == null)
                throw new ArgumentException("user");

            _context.UserRoles.Remove(role);
            await _context.SaveChangesAsync();
        }

        public async TaskThreading.Task<IEnumerable<UserRole>> GetItems()
        {
            return await TaskThreading.Task.Run(() => _context.UserRoles);
        }

        public async TaskThreading.Task<IQueryable<UserRole>> GetItemsQueryable()
        {
            return await TaskThreading.Task.Run(() => _context.UserRoles);
        }

    }
}
