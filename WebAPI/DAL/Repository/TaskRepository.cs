﻿using AutoMapper;
using DAL.Context;
using DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TaskThreading = System.Threading.Tasks;

namespace DAL.Repository
{
    public class TaskRepository : IRepository<Task>
    {
        private readonly InitialDataContext _context;
        public TaskRepository(InitialDataContext context)
        {
            _context = context;
        }
        public async TaskThreading.Task Add(Task item)
        {
            if (item is null)
                throw new ArgumentNullException("item");

            var task = await _context.Tasks.FirstOrDefaultAsync(x => x.Id == item.Id);
            if (task != null)
            {
                var lastElem = await _context.Tasks.OrderBy(x => x.Id).LastAsync();
                var nextIndex = lastElem.Id;
                item.Id = ++nextIndex;
            }

            await _context.Tasks.AddAsync(item);
            await _context.SaveChangesAsync();
        }

        public async TaskThreading.Task AddRange(IEnumerable<Task> range)
        {
            if (range is null)
                throw new ArgumentNullException("range");

            foreach (var item in range)
            {
                if (await _context.Tasks.AnyAsync(x => x.Id == item.Id))
                    throw new ArgumentException("range");
            }
            await _context.Tasks.AddRangeAsync(range);
            await _context.SaveChangesAsync();
        }

        public async TaskThreading.Task Delete(Task item)
        {
            if (item is null)
                throw new ArgumentNullException("item");

            _context.Tasks.Remove(item);
            await _context.SaveChangesAsync();
        }

        public async TaskThreading.Task DeleteById(int id)
        {
            var task = await _context.Tasks.FirstOrDefaultAsync(x => x.Id == id);
            if (task == null)
                throw new ArgumentException("id");

            _context.Tasks.Remove(task);
            await _context.SaveChangesAsync();
        }

        public async TaskThreading.Task<Task> GetItem(int id)
        {
            var task = await _context.Tasks.FirstOrDefaultAsync(x => x.Id == id);
            if (task == null)
                throw new ArgumentException("id");

            return await _context.Tasks
                .Include(x => x.Project)
                .Include(x => x.Performer)
                .FirstOrDefaultAsync(x => x.Id == id);
        }

        public async TaskThreading.Task<IQueryable<Task>> GetItemsQueryable()
        {
            return _context.Tasks;
        }

        public async TaskThreading.Task Update(Task item)
        {
            if (item is null)
                throw new ArgumentNullException("item");

            var task = await _context.Tasks.FirstOrDefaultAsync(x => x.Id == item.Id);
            if (task is null)
                throw new ArgumentException("item");
            // it is must have, cause ef throws exeption that instance of entity type cannot be tracked 
            // because another instance with the same key value for {'Id'} is already being tracked
            _context.Entry<Task>(task).State = EntityState.Detached;
            _context.Entry<Task>(item).State = EntityState.Detached;

            task.Name = item.Name;
            task.Description = item.Description;
            task.State = item.State;
            _context.Tasks.Update(task);
            await _context.SaveChangesAsync();
        }

        public async TaskThreading.Task<IEnumerable<Task>> GetItems()
        {
            return  _context.Tasks;
        }
    }
}
