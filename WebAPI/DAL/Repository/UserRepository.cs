﻿using AutoMapper;
using DAL.Context;
using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using TaskThreading = System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Security.Cryptography.X509Certificates;

namespace DAL.Repository
{
    public class UserRepository : IRepository<User>
    {
        private readonly InitialDataContext _context;
        public UserRepository(InitialDataContext context)
        {
            _context = context;
        }
        
        public async TaskThreading.Task Add(User item)
        {
            if (item is null)
                throw new ArgumentNullException("item");


            var user = await _context.Users.FirstOrDefaultAsync(x => x.Id == item.Id);
            if (user != null)
            {
                var nextIndex = _context.Users.OrderBy(x => x.Id).Last().Id;
                item.Id = ++nextIndex;
            }

            await _context.Users.AddAsync(item);
            await _context.SaveChangesAsync();
        }

        public async TaskThreading.Task AddRange(IEnumerable<User> range)
        {
            if (range is null)
                throw new ArgumentNullException("range");

            foreach (var item in range)
            {
                if (await _context.Users.AnyAsync(x => x.Id == item.Id))
                    throw new ArgumentException("range");
            }

            await _context.Users.AddRangeAsync(range);
            await _context.SaveChangesAsync();
        }

        public async TaskThreading.Task Delete(User item)
        {
            if (item is null)
                throw new ArgumentNullException("item");

            _context.Users.Remove(item);
            await _context.SaveChangesAsync();
        }

        public async TaskThreading.Task DeleteById(int id)
        {
            var user = await _context.Users.FirstOrDefaultAsync(x => x.Id == id);
            if (user == null)
                throw new ArgumentException("user");

            _context.Users.Remove(user);
            await _context.SaveChangesAsync();
        }

        public async TaskThreading.Task<User> GetItem(int id)
        {
            var user = await _context.Users.FirstOrDefaultAsync(x => x.Id == id);
            if (user == null)
                throw new ArgumentException("user");

            return await _context.Users
                .Include(x => x.Projects)
                .Include(x => x.Tasks)
                .Include(x => x.Team)
                .FirstOrDefaultAsync(x => x.Id == id);
        }

        public async TaskThreading.Task<IEnumerable<User>> GetItems()
        {
            return _context.Users;
        }

        public async TaskThreading.Task<IQueryable<User>> GetItemsQueryable()
        {
            return  _context.Users;
        }

        public async TaskThreading.Task Update(User item)
        {
            var user = await _context.Users.FirstOrDefaultAsync(x => x.Id == item.Id);
            if (user == null)
                throw new ArgumentException("id");

            // it is must have, cause ef throws exeption that instance of entity type cannot be tracked 
            // because another instance with the same key value for {'Id'} is already being tracked
            _context.Entry<User>(user).State = EntityState.Detached;
            _context.Entry<User>(item).State = EntityState.Detached;
            user.FirstName = item.FirstName;
            user.LastName = item.LastName;
            user.EmailAddress = item.EmailAddress;
            _context.Users.Update(user);
            await _context.SaveChangesAsync();
        }

        
    }
}
