﻿using AutoMapper;
using DAL.Context;
using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using TaskThreading = System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace DAL.Repository
{
    public class TeamRepository : IRepository<Team>
    {
        private readonly InitialDataContext _context;
        public TeamRepository(InitialDataContext context)
        {
            _context = context;
        }
        public async TaskThreading.Task Add(Team item)
        {
            if (item is null)
                throw new ArgumentNullException("item");

            var team = await _context.Teams.FirstOrDefaultAsync(x => x.Id == item.Id);
            if (team != null)
            {
                var nextIndex = _context.Teams.OrderBy(x => x.Id).Last().Id;
                item.Id = ++nextIndex;
            }

            await _context.Teams.AddAsync(item);
            await _context.SaveChangesAsync();
        }

        public async TaskThreading.Task AddRange(IEnumerable<Team> range)
        {
            if (range is null)
                throw new ArgumentNullException("range");

            foreach (var item in range)
            {
                if (await _context.Teams.AnyAsync(x => x.Id == item.Id))
                    throw new ArgumentException("range");
            }

            await _context.Teams.AddRangeAsync(range);
            await _context.SaveChangesAsync();
        }

        public async TaskThreading.Task Delete(Team item)
        {
            if (item is null)
                throw new ArgumentNullException("item");

            _context.Teams.Remove(item);
            await _context.SaveChangesAsync();
        }

        public async TaskThreading.Task DeleteById(int id)
        {
            var team = await _context.Teams.FirstOrDefaultAsync(x => x.Id == id);
            if (team == null)
                throw new ArgumentException("id");

            _context.Teams.Remove(team);
            await _context.SaveChangesAsync();
        }

        public async TaskThreading.Task<Team> GetItem(int id)
        {
            var team = await _context.Teams.FirstOrDefaultAsync(x => x.Id == id);
            if (team == null)
                throw new ArgumentException("id");

            return await _context.Teams
                .Include(x => x.Projects)
                .Include(x => x.Users)
                .FirstOrDefaultAsync(x => x.Id == id);
        }

        public async TaskThreading.Task<IEnumerable<Team>> GetItems()
        {
            return _context.Teams;
        }

        public async TaskThreading.Task<IQueryable<Team>> GetItemsQueryable()
        {
            return  _context.Teams;
        }

        public async TaskThreading.Task Update(Team item)
        {
            var team = await _context.Teams.FirstOrDefaultAsync(x => x.Id == item.Id);
            if (team == null)
                throw new ArgumentException("item");

            // it is must have, cause ef throws exeption that instance of entity type cannot be tracked 
            // because another instance with the same key value for {'Id'} is already being tracked
            _context.Entry<Team>(team).State = EntityState.Detached;
            _context.Entry<Team>(item).State = EntityState.Detached;
            team.Name = item.Name;
            _context.Teams.Update(team);
            await _context.SaveChangesAsync();
        }
    }
}
