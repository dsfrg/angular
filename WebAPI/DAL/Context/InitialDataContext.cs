﻿using DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Context
{
    public class InitialDataContext: DbContext
    {
        public InitialDataContext(DbContextOptions<InitialDataContext> options)
            :base(options)
        { }

        public DbSet<Project> Projects { get; set; }
        public DbSet<Task> Tasks { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<TaskStateModel> TaskStates { get; set; }
        public DbSet<UserRole> UserRoles { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Project>()
                .HasMany(p => p.Tasks)
                .WithOne(c => c.Project)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Task>()
                .HasOne(x => x.Project)
                .WithMany(x => x.Tasks)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Task>()
                .Property(t => t.Name)
                .IsRequired();
            
            modelBuilder.Entity<Task>()
                .Property(t => t.Name)
                .HasMaxLength(1000);


            modelBuilder.Seed();
        }
    }
}
