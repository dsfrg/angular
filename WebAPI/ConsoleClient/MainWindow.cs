﻿using Common.DTOs;
using Common.DTOs.QueryDtos;
using ConsoleClient.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace LINQ_FirstLecture
{
    public class MainWindow
    {
        private string MainMenu = "1 - Get dictionary(key - project, value - count of tasks) by user id\n" +
            "2 - List of tasks for user\n" +
            "3 - Get (id, name) from list of tasks which are finished by user id\n" +
            "4 - Get list (id, team name and list of users)  from list of teams\n" +
            "5 - Get list of users by first_name with sorted tasks by name length\n" +
            "6- Get CombinedInfoAboutUserTask6\n" +
            "7- Get CombinedInfoAboutProjectTask7\n" +
            "8 - Add user\n" +
            "9 - Add task\n" +
            "10 - Add Team\n" +
            "11 - Add project\n" +
            "12 - Get project by id\n" +
            "13 - Get task by id\n" +
            "14 - Get user by id\n" +
            "15 - Get team by id\n" +
            "16 - Get all projects\n" +
            "17 -Get all tasks\n" +
            "18 - Get all users\n" +
            "19 - Get all teams\n" +
            "20 - Delete project by id\n" +
            "21 - Delete task by id\n" +
            "22 - Delete user by id\n" +
            "23 - Delete team by id\n" +
            "24 - Mark random task as finished\n" +
            "-1 to exit\n";
        TimerService timer;
        public int GetId()
        {
            int.TryParse(Console.ReadLine(), out int id);
            while (id == 0)
            {
                Console.WriteLine("Enter id once more");
                int.TryParse(Console.ReadLine(), out id);
            }
            Console.Clear();
            return id;
        }

        public int GetAnswer()
        {
            Console.WriteLine("Enter your answer id");
            int.TryParse(Console.ReadLine(), out int id);
            if (id == -1)
                System.Environment.Exit(-1);

            while (id < 1 || id > 24)
            {
                Console.WriteLine("Enter correct data");
                int.TryParse(Console.ReadLine(), out id);
            }

            Console.Clear();
            return id;
        }

        public void WantToContinue()
        {
            Console.WriteLine("\n\nDo you want to go on ?");
            string answer = Console.ReadLine();
            if (answer == "y" || answer == "Y")
                Console.Clear();
            else
                System.Environment.Exit(-2);
        }

        public async Task Start()
        {
            Console.WriteLine(MainMenu);
            int answer = GetAnswer();
            while (answer != -1)
            {
                switch (answer)
                {
                    case 1:
                        {
                            await DisplayDictionaryProjectToTaskCountByUserId();
                            WantToContinue();
                            break;
                        }
                    case 2:
                        {
                            await DisplayTasksByUserIdWhereNameLessThan45();
                            WantToContinue();
                            break;
                        }
                    case 3:
                        {
                            await DisplayCollectionOfTasksWhichAreFinished();
                            WantToContinue();
                            break;
                        }
                    case 4:
                        {
                            await DisplayTeamsOlderThan10Years();
                            WantToContinue();
                            break;
                        }
                    case 5:
                        {
                            await DisplayListOfUsers();
                            WantToContinue();
                            break;
                        }
                    case 6:
                        {
                            await GetTask6Structure();
                            WantToContinue();
                            break;
                        }
                    case 7:
                        {
                            await GetTask7Structure();
                            WantToContinue();
                            break;
                        }
                    case 8:
                        {
                            await SendUser();
                            WantToContinue();
                            break;
                        }
                    case 9:
                        {
                            await SendTask();
                            WantToContinue();
                            break;
                        }
                    case 10:
                        {
                            await SendTeam();
                            WantToContinue();
                            break;
                        }
                    case 11:
                        {
                            await SendProject();
                            WantToContinue();
                            break;
                        }
                    case 12:
                        {
                            Console.WriteLine("Enter id:");
                            await GetSingleProject(GetId());
                            WantToContinue();
                            break;
                        }
                    case 13:
                        {
                            Console.WriteLine("Enter id:");
                            await GetSingleTask(GetId());
                            WantToContinue();
                            break;
                        }
                    case 14:
                        {
                            Console.WriteLine("Enter id:");
                            await GetSingleUser(GetId());
                            WantToContinue();
                            break;
                        }
                    case 15:
                        {
                            Console.WriteLine("Enter id:");
                            await GetSingleTeam(GetId());
                            WantToContinue();
                            break;
                        }
                    case 16:
                        {
                            var proj = JsonConvert.DeserializeObject<IEnumerable<ProjectDto>>(await HttpService.GetStringResponce("project")).ToList();
                            for (int i = 0; i < proj.Count; i++)
                                Console.WriteLine($"Project id: {proj[i].Id}, Project name: {proj[i].Name}");
                            WantToContinue();
                            break;
                        }
                    case 17:
                        {
                            var tasks = JsonConvert.DeserializeObject<IEnumerable<TaskDto>>(await HttpService.GetStringResponce("task")).ToList();
                            for (int i = 0; i < tasks.Count; i++)
                                Console.WriteLine($"Task id: {tasks[i].Id}, task name: {tasks[i].Name}");
                            WantToContinue();
                            break;
                        }
                    case 18:
                        {
                            var users = JsonConvert.DeserializeObject<IEnumerable<UserDto>>(await HttpService.GetStringResponce("user")).ToList();
                            for (int i = 0; i < users.Count; i++)
                                Console.WriteLine($"User name: {users[i].FirstName}, User id: {users[i].Id}, user last name: {users[i].LastName}");
                            WantToContinue();
                            break;
                        }
                    case 19:
                        {
                            var teams = JsonConvert.DeserializeObject<IEnumerable<TeamDto>>(await HttpService.GetStringResponce("team")).ToList();
                            for (int i = 0; i < teams.Count; i++)
                                Console.WriteLine($"Team id: {teams[i].Id}, team name: {teams[i].Name}");
                            WantToContinue();
                            break;
                        }
                    case 20:
                        {
                            Console.WriteLine("Enter id:");
                            await HttpService.Delete($"project/{GetId()}");
                            WantToContinue();
                            break;
                        }
                    case 21:
                        {
                            Console.WriteLine("Enter id:");
                            await HttpService.Delete($"task/{GetId()}");
                            WantToContinue();
                            break;
                        }
                    case 22:
                        {
                            Console.WriteLine("Enter id:");
                            await HttpService.Delete($"user/{GetId()}");
                            WantToContinue();
                            break;
                        }
                    case 23:
                        {
                            Console.WriteLine("Enter id:");
                            await HttpService.Delete($"team/{GetId()}");
                            WantToContinue();
                            break;
                        }
                    case 24:
                        {
                            timer = new TimerService(1000);
                            timer.Elapsed += Timer_Elapsed;
                            timer.StartReadout();
                            break;
                        }
                    default:
                        {
                            Console.WriteLine("Incorect answer");
                            WantToContinue();
                            break;
                        }
                }

                Console.WriteLine(MainMenu);
                answer = GetAnswer();
            }
            Console.WriteLine("Bye - bye");
        }

        private async void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                int id = await MarkAsFinishedRandomTask();
                Console.WriteLine($"Finished task {id}");
            }
            catch (Exception)
            {

                throw;
            }
            timer.Stop();
        }
        public async Task<int> MarkAsFinishedRandomTask()
        {
            var json = await HttpService.GetStringResponce("task");
            var tasks = JsonConvert.DeserializeObject<IEnumerable<TaskDto>>(json)
                .Where(x => x.State != TaskStateDto.Finished)
                .ToList();

            var random = new Random();
            var randomIndex = random.Next(0, tasks.Count);
            var taskCompletionSource = new TaskCompletionSource<int>();
            var response = await HttpService.PutData("task/markFinished", tasks[randomIndex]);
            if (response.IsSuccessStatusCode)
            {
                taskCompletionSource.SetResult(tasks[randomIndex].Id);
            }
            else
                taskCompletionSource.SetException(new ArgumentException());

            return await taskCompletionSource.Task;
        }

        public  async Task DisplayDictionaryProjectToTaskCountByUserId()
        {
            Console.WriteLine("Enter user id");
            int id = GetId();
            var stringResponce = await HttpService.GetStringResponce(@$"query/projecttotask/{id}");
            var result = JsonConvert.DeserializeObject<IEnumerable<ProjectToTaskCountStructureDto>>(stringResponce)
                .                   ToDictionary(x => x.Project, x => x.CountOfTasks);
            foreach (var item in result)
            {
                Console.WriteLine($"Project: {item.Key.Name}, Task count: {item.Value}");
            }
        }

        

        public async Task DisplayTasksByUserIdWhereNameLessThan45()
        {
            Console.WriteLine("Enter user id");
            int id = GetId();
            var stringResponce = await HttpService.GetStringResponce(@$"query/usertaknameless45/{id}");
            var result = JsonConvert.DeserializeObject<IEnumerable<TaskDto>>(stringResponce);
            foreach (var item in result)
            {
                Console.WriteLine($"Task name: {item.Name}, task id: {item.Id}");
            }
        }

        public async Task DisplayCollectionOfTasksWhichAreFinished()
        {
            Console.WriteLine("Enter user id");
            int id = GetId();
            var stringResponce = await HttpService.GetStringResponce(@$"query/finishedtasksidname/{id}");
            var result = JsonConvert.DeserializeObject<IEnumerable<TasksIdNameDto>>(stringResponce);
            foreach (var item in result)
            {
                Console.WriteLine($"Finished task: Name = {item.Name}, Id = {item.Id}");
            }
        }

        public async Task DisplayTeamsOlderThan10Years()
        {
            var stringResponce = await HttpService.GetStringResponce(@$"query/teamswithusers");
            var result = JsonConvert.DeserializeObject<IEnumerable<TeamUsersDto>>(stringResponce);
            foreach (var item in result)
            {
                Console.WriteLine($"id = {item.TeamId}  name = {item.TeamName}");
                foreach (var user in item.Users)
                {
                    Console.WriteLine($"User name: {user.FirstName}");
                }
            }
        }

        public async Task DisplayListOfUsers()
        {
            var stringResponce = await HttpService.GetStringResponce(@$"query/userswithtasks");
            var result = JsonConvert.DeserializeObject<IEnumerable<UserTasksDto>>(stringResponce);
            foreach (var item in result)
            {
                Console.WriteLine($"id = {item.User.Id}  name = {item.User.FirstName}");
                foreach (var task in item.Tasks)
                {
                    Console.WriteLine($"Task name {task.Name}");
                }
            }
        }

        public async Task GetTask6Structure()
        {
            Console.WriteLine("Enter user id");
            int id = GetId();
            var stringResponce = await HttpService.GetStringResponce(@$"query/infoaboutuser/{id}");
            Console.WriteLine(stringResponce);
        }

        public async Task GetTask7Structure()
        {
            var stringResponce = await HttpService.GetStringResponce(@$"query/GetProjectInfo");
            Console.WriteLine(stringResponce);
        }

        public UserDto GetUser()
        {
            UserDto user = new UserDto();
            Console.WriteLine("Enter id:");
            user.Id = Int32.Parse(Console.ReadLine());

            Console.WriteLine("Enter First name:");
            user.FirstName = Console.ReadLine();

            Console.WriteLine("Enter Last name:");
            user.LastName = Console.ReadLine();

            Console.WriteLine("Enter Email:");
            user.EmailAddress = Console.ReadLine();
            user.RegisteredAt = DateTime.Now;

            Console.WriteLine("Enter Birthaday date:");
            user.BirthdayDate = DateTime.Parse(Console.ReadLine());


            //Console.WriteLine("Enter team id:");
            //user.TeamId = Int32.Parse(Console.ReadLine());

            Console.Clear();
            return user;
        }

        public TaskDto GetTask()
        {
            TaskDto task = new TaskDto();

            Console.WriteLine("Enter id:");
            task.Id = Int32.Parse(Console.ReadLine());

            Console.WriteLine("Enter  name:");
            task.Name = Console.ReadLine();

            Console.WriteLine("Enter description:");
            task.Description = Console.ReadLine();
            task.CreatedAt = DateTime.Now;
            task.FinishedAt = DateTime.Now;

            Console.WriteLine("Enter task state(from 0 to 3):");
            task.State = (TaskStateDto)Int32.Parse(Console.ReadLine());

            //Console.WriteLine("Enter project id: ");
            //task.ProjectId = Int32.Parse(Console.ReadLine());

            //Console.WriteLine("Enter performer id:");
            //task.PerformerId = Int32.Parse(Console.ReadLine());

            Console.Clear();
            return task;
        }


        public TeamDto GetTeam()
        {
            TeamDto team = new TeamDto();

            Console.WriteLine("Enter id:");
            team.Id = Int32.Parse(Console.ReadLine());

            Console.WriteLine("Enter  name:");
            team.Name = Console.ReadLine();
            team.CreatedAt = DateTime.Now;

            return team;

        }


        public ProjectDto GetProject()
        {
            ProjectDto project = new ProjectDto();


            Console.WriteLine("Enter id:");
            project.Id = Int32.Parse(Console.ReadLine());

            Console.WriteLine("Enter  name:");
            project.Name = Console.ReadLine();
            project.CreatedAt = DateTime.Now;
            project.Deadline = DateTime.Now;

            Console.WriteLine("Enter  description:");
            project.Description = Console.ReadLine();

            //Console.WriteLine("Enter author id: ");
            //project.AuthorId = Int32.Parse(Console.ReadLine());

            //Console.WriteLine("Enter project id:");
            //project.TeamId = Int32.Parse(Console.ReadLine());

            return project;
        }

        public async Task SendUser()
        {
            var user = GetUser();
            await HttpService.PostData("user", user);
        }

        public async Task SendTeam()
        {
            var team = GetTeam();
            await HttpService.PostData("team", team);
        }

        public async Task SendTask()
        {
            var task = GetTask();
            await HttpService.PostData("task", task);
        }


        public async Task SendProject()
        {
            var project = GetProject();
            await HttpService.PostData("project", project);
        }

        public async Task GetSingleProject(int id)
        {
            var project = JsonConvert.DeserializeObject<ProjectDto>(await HttpService.GetStringResponce($"project/{id}"));
            Console.WriteLine($"Project id: {project.Id}, Project name: {project.Name}");
        }

        public async Task GetSingleUser(int id)
        {
            var user = JsonConvert.DeserializeObject<UserDto>(await HttpService.GetStringResponce($"user/{id}"));
            Console.WriteLine($"User name: {user.FirstName}, User id: {user.Id}, user last name: {user.LastName}");
        }

        public async Task GetSingleTask(int id)
        {
            var task = JsonConvert.DeserializeObject<TaskDto>(await HttpService.GetStringResponce($"task/{id}"));
            Console.WriteLine($"Task id: {task.Id}, task name: {task.Name}");
        }

        public async Task GetSingleTeam(int id)
        {
            var team = JsonConvert.DeserializeObject<TeamDto>(await HttpService.GetStringResponce($"team/{id}"));
            Console.WriteLine($"Team id: {team.Id}, team name: {team.Name}");
        }

    }
}
