﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleClient.Services
{
    public static class HttpService
    {
        private const string URL = @"http://localhost:56122/api/";
        public async static Task<string> GetStringResponce(string api)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                var responseBody = await httpClient.GetAsync(URL + api);
                if (!responseBody.IsSuccessStatusCode)
                {
                    Console.WriteLine("Server is not available");
                }
                return await responseBody.Content.ReadAsStringAsync();
            }
        }

        public async static Task PostData(string api, object data)
        {
            string json  = JsonConvert.SerializeObject(data);
            using (var client = new HttpClient())
            {
                await client.PostAsync(URL + api, new StringContent(json, Encoding.UTF8, "application/json"));
            }
        }

        public async static Task<HttpResponseMessage> PutData(string api, object data)
        {
            string json = JsonConvert.SerializeObject(data);
            using (var client = new HttpClient())
            {
                var response = await client.PutAsync(URL + api, new StringContent(json, Encoding.UTF8, "application/json"));
                return response;
            }
        }


        public async static Task Delete(string api)
        {
            using (var client = new HttpClient())
            {
                await client.DeleteAsync(URL + api);
            }
        }
    }
}
