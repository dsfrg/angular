﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs
{
    public class UserDto
    {
        public int Id { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? EmailAddress { get; set; }

        public DateTime BirthdayDate { get; set; }
        public DateTime RegisteredAt { get; set; }
        public TeamDto Team { get; set; }

        public UserRoleDto Role { get; set; }

        public ICollection<TaskDto> Tasks { get; set; }
        public ICollection<ProjectDto> Projects { get; set; }
    }
}
