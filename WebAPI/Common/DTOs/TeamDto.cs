﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace Common.DTOs
{
    public class TeamDto
    {
        public int Id { get; set; }
        public string? Name { get; set; }

        public DateTime CreatedAt { get; set; }

        public virtual ICollection<UserDto> Users { get; set; }
        public virtual ICollection<ProjectDto> Projects { get; set; }
    }
}
