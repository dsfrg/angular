﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs
{
    public class TaskDto
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }

        public DateTime CreatedAt { get; set; }
        public DateTime FinishedAt { get; set; }
        public TaskStateDto State { get; set; }
        public ProjectDto Project { get; set; }

        public UserDto Performer { get; set; }

    }
}
