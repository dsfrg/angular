﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.Services;
using Common.DTOs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TaskThreading = System.Threading.Tasks;

namespace WebAPI.App.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamController : ControllerBase
    {
        private readonly TeamService _teamService;

        public TeamController(TeamService teamService)
        {
            _teamService = teamService;
        }

        [HttpGet]
        public async TaskThreading.Task<ActionResult<IEnumerable<TeamDto>>> GetAllTeams()
        {
            return Ok(await _teamService.GetTeams());
        }

        [HttpGet("{id}")]
        public async TaskThreading.Task<ActionResult<TeamDto>> GetSingleTeam(int id)
        {
            return Ok(await _teamService.GetTeam(id));
        }


        [HttpPost]
        public async TaskThreading.Task<IActionResult> AddSingleTeam(TeamDto team)
        {
            await _teamService.AddTeam(team);
            return Ok();
        }

        [HttpPost("range")]
        public async TaskThreading.Task<IActionResult> AddTeams(IEnumerable<TeamDto> teams)
        {
            await _teamService.AddRangeOfTeams(teams);
            return Ok();
        }

        [HttpDelete]
        public async TaskThreading.Task<IActionResult> DeleteTeam(TeamDto team)
        {
            await _teamService.Delete(team);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public async TaskThreading.Task<IActionResult> DeleteTeamById(int id)
        {
            await _teamService.DeleteById(id);
            return NoContent();
        }


        [HttpPut]
        public async TaskThreading.Task<IActionResult> ChangeTeam(TeamDto team)
        {
            await _teamService.Update(team);
            return Ok();
        }
    }
}
