﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.Services;
using Common.DTOs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TaskThreading = System.Threading.Tasks;

namespace WebAPI.App.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectController : ControllerBase
    {
        private readonly ProjectService _projectService;

        public ProjectController(ProjectService projectService)
        {
            _projectService = projectService;
        }

        [HttpGet]
        public async TaskThreading.Task<ActionResult<IEnumerable<ProjectDto>>> GetAllProjects()
        {
            return Ok(await _projectService.GetProjects());
        }

        [HttpGet("{id}")]
        public async TaskThreading.Task<ActionResult<ProjectDto>> GetSingleProject(int id)
        {
            var a = await _projectService.GetProject(id);
            return Ok(await _projectService.GetProject(id));
        }


        [HttpPost("add")]
        public async TaskThreading.Task<IActionResult> AddSingleProject(ProjectDto project)
        {
            await _projectService.AddProject(project);
            return Ok();
        }

        [HttpPost("range")]
        public async TaskThreading.Task<IActionResult> AddProjects(IEnumerable<ProjectDto> projects)
        {
            await _projectService.AddRangeOfProjects(projects);
            return Ok();
        }

        [HttpDelete]
        public async TaskThreading.Task<IActionResult> DeleteProject(ProjectDto project)
        {
            await _projectService.Delete(project);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public async TaskThreading.Task<IActionResult> DeleteProjectById(int id)
        {
            await _projectService.DeleteById(id);
            return NoContent();
        }


        [HttpPut]
        public async TaskThreading.Task<IActionResult> ChangeProject(ProjectDto project)
        {
            await _projectService.Update(project);
            return Ok();
        }
    }
}
