﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.Services;
using Common.DTOs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TaskThreading = System.Threading.Tasks;

namespace WebAPI.App.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserRoleController : ControllerBase
    {
        private readonly UserRoleService _service;

        public UserRoleController(UserRoleService service)
        {
            _service = service;
        }

        [HttpGet]
        public async TaskThreading.Task<ActionResult<IEnumerable<UserRoleDto>>> GetAllRoless()
        {
            return Ok(await _service.GetUserRoles());
        }

        [HttpGet("{id}")]
        public async TaskThreading.Task<ActionResult<UserRoleDto>> GetSingleUserRole(int id)
        {
            return Ok(await _service.GetUserRole(id));
        }


        [HttpPost]
        public async TaskThreading.Task<IActionResult> AddSingleRole(UserRoleDto role)
        {
            await _service.AddRole(role);
            return Ok();
        }

        [HttpPost("range")]
        public async TaskThreading.Task<IActionResult> AddUsers(IEnumerable<UserRoleDto> roles)
        {
            await _service.AddRangeOfRoles(roles);
            return Ok();
        }

        [HttpDelete]
        public async TaskThreading.Task<IActionResult> DeleteRole(UserRoleDto role)
        {
            await _service.Delete(role);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public async TaskThreading.Task<IActionResult> DeleteRole(int id)
        {
            await _service.DeleteById(id);
            return NoContent();
        }


        [HttpPut]
        public async TaskThreading.Task<IActionResult> ChangeRole(UserRoleDto role)
        {
            await _service.Update(role);
            return Ok();
        }
    }
}
